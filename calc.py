import sys

def add(num1, num2):
    return num1 + num2

def main(op, num1, num2):
    if op == "add":
        return add(num1, num2)
    elif op == "subtract":
        return "Not implemented yet"
    elif op == "multiply":
        return "Not implemented yet"
    elif op == "divide":
        return "Not implemented yet"
    else:
        return "Not implemented yet"

if __name__ == "__main__":
    if len(sys.argv) != 4:
        exit("Not enough parameters")
    _, op, num1, num2 = sys.argv
    main(op, num1, num2)
