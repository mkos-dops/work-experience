# Calculator

## Environment and dependencies

1. Open Visual Studio Code
2. Go to Terminal -> New Terminal
3. In a terminal window type:
 - python
 - pip
 - git

Once everything is working we are set to go

## Using pip

```sh
# list packages already installed
pip list
# install requirements dependencies
pip install -r requirements.txt
```

## Checking the calc project

[GitLab project](https://gitlab.com/mkos-dops/work-experience)

```sh
git clone git@gitlab.com:mkos-dops/work-experience.git
cd work-experience
git checkout main
```

## Using git

```sh
# which files has been changed?
git status
# what changes have been done?
git diff
# accept all of the them
git add .
# create a commit
git commit -m "write description here"
# push my commits to central repository
git push origin <branch name>
# create a new branch
git checkout -b <branch name>
```

## Running the tests

```sh
# running all the tests
python3 -m pytest
# running specific test
python3 -m pytest -k test_dummy
```

## Using the calculator

```sh
python3 calc.py add 10 10
python3 calc.py subtract 10 10
python3 calc.py multiply 10 10
python3 calc.py divide 10 10
```